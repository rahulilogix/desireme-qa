<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\MinkExtension\Context\MinkContext;
/**
 * Defines application features from the specific context.
 */
class FeatureContext extends \Behat\MinkExtension\Context\MinkContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given /^check login is success or not$/
     * @Given /^Wait for response$/
     */
    public function checkLoginIsSuccessOrNot()
    {
        sleep(5);

        //*[@id="radio_1"]
        $page = $this->getSession()->getPage();
        try {
            $error_model = $this->assertSession()->elementExists('css', 'body > div.fade.custom__modal.modal.show > div > div');
            if ($error_model != Null) {
                $error = $this->assertSession()->elementExists('css', 'body > div.fade.custom__modal.modal.show > div > div > div.modal-body > div > div > p')->getText();
                if ($error != Null) {
                    echo $error;
                } else {
                    echo"login or register is done";
                }
            }
        }
        catch (\Behat\Mink\Exception\ElementNotFoundException $e){
            $e->getTrace();
            echo "login or register is done";
        }
    }
}
