Feature: login
  Background:
    Given I am on "https://staging.desire.me"
@register
  Scenario: test register page
    When I am on "/profile"
    Then I fill in "Forename" with "test"
    Then I fill in "Surname" with "test"
    Then I fill in "Email" with "test@gmail.com"
    Then I fill in "Username" with "test"
    Then I fill in "DisplayName" with "test"
    Then I fill in "PhoneNumber" with "70165879466"
    Then I fill in "Password" with "Test@2112"
    Then I fill in "ConfirmPassword" with "Test@2112"
    Then I fill in "Category" with "Male"
    Then I check "AgreeTerms"
    Then I check "YearsOld"
    Then I press "Next"
    And check login is success or not

@login
  Scenario Outline: Login
    When I am on "/login"
    And I fill in "email" with "<email>"
    And I fill in "password" with "<password>"
    Examples:
    |email|password|
    |rahul@ilogixinfotech.com|123   |
    |rahul@ilogixinfotech.com|123456|
    |test2112@gmail.com      |123   |
    |rahul@ilogixinfotech.com|      |
    |                        |123456|
    |                        |      |
    And I press "SUBMIT"
    And check login is success or not
    Then I should see "Logout"

@contact
  Scenario: ContactUS
    When I am on "/login"
    And I fill in "email" with "rahul@ilogixinfotech.com"
    And I fill in "password" with "123456"
    And I am on "/contact-us"
    Then I fill in "Fullname" with "test"
    Then I fill in "Email" with "test@gmail.com"
    Then I fill in "message" with "test"
    Then I press "Submit"
    And Wait for response