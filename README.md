step 1: Install PHP >= 5.3.1 in your system
	https://ubuntu.com/server/docs/programming-php
	
step 2: Install Composer in the system
	https://getcomposer.org/download
	
step 3: create folder with "behat" name (with any name)
		add composer.json and behat.yml file in folder
		
step 4: Execute “composer install” from your terminal. This command will read the ‘composer.json'

step 5: add features folder in behat folder

step 6: chrome should be install in system for use chromedriver
		run this command for run chrome driver:=	
                       "google-chrome-stable --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222"
For more details := https://mink.behat.org/en/latest/drivers/chrome.html

step 7: run behat 
	for registration := "vendor/bin/behat --tags @register"
	for login := "vendor/bin/behat --tags @login"
	for ContactUs := "vendor/bin/behat --tags @contac"
	for all := "vendor/bin/behat"


